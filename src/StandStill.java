import bos.NoMove;
import bos.RelativeMove;

import java.util.List;

public class StandStill implements Behaviour {
    @Override
    public RelativeMove chooseMove(Stage stage, Character mover) {
        return new NoMove(stage.grid, mover);
    }

    public List<RelativeMove> getMoves(){
        return null;
    }
}
