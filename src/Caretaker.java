import bos.GameBoard;

public  class Caretaker implements KeyObserver {

    Stage stage;
    Memento saveMemento;


    public Caretaker(Stage stage) {
        this.stage = stage;
    }

    public void notify(char c, GameBoard<Cell> gb) {

        if(c == ' ') {
            Memento m = stage.createMemento();

           saveMemento = new Memento(stage.getMemento(m).sheep, stage.getMemento(m).shepherd, stage.getMemento(m).shepherd, stage.getMemento(m).player);
            System.out.println("Game saved!");

        }

       if(c == 'r') {
                if(saveMemento == null){
                    System.out.println("You can't load because you haven't saved!");
                }
                else {
                    stage.setMemento(saveMemento);
                    System.out.println("Game loaded!");
                }





        }

    }





}
