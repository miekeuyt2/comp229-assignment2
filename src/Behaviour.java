import bos.RelativeMove;

import java.util.List;

public interface Behaviour {
    public RelativeMove chooseMove(Stage stage, Character mover);
    public List<RelativeMove> getMoves();
}
