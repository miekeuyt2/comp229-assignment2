import java.awt.*;
import java.util.*;
import java.time.*;
import java.util.List;

import bos.*;

public class Stage extends KeyObservable {
    protected Grid grid;
    protected Character sheep;
    protected Character shepherd;
    protected Character wolf;
    protected Block block;
    protected Block[] blocks;
    Block block2;
    private List<Character> allCharacters;
    protected Player player;
    Caretaker care;
    private Instant timeOfLastMove = Instant.now();
    Memento mem;

    public Stage() {
        SAWReader sr = new SAWReader("data/stage1.saw");
        grid     = new Grid(10, 10);
        shepherd = new Shepherd(grid.cellAtRowCol(sr.getShepherdLoc().first, sr.getShepherdLoc().second), new StandStill());
        sheep    = new Sheep(grid.cellAtRowCol(sr.getSheepLoc().first, sr.getSheepLoc().second), new MoveTowards(shepherd));
        wolf     = new Wolf(grid.cellAtRowCol(sr.getWolfLoc().first, sr.getWolfLoc().second), new MoveTowards(sheep));
         blocks = new Block[sr.getBlockLoc().size()];
        for(int i = 0; i < blocks.length; i++){
            blocks[i] =  new Block( grid.cellAtRowCol(sr.getBlockLoc().get(i).first, sr.getBlockLoc().get(i).second));
        }
        player = new Player(grid.cellAtRowCol(sr.getPlayerLoc().first, sr.getPlayerLoc().second));


//        player = new Player(grid.getRandomCell());

        care = new Caretaker(this);
   mem = new Memento();
        this.register(player);
        this.register(care);


        allCharacters = new ArrayList<Character>();
        allCharacters.add(sheep); allCharacters.add(shepherd); allCharacters.add(wolf);

    }

    public void update(){
        if (!player.inMove()) {
            if (sheep.location == shepherd.location) {
                System.out.println("The sheep is safe :)");
                System.exit(0);
            } else if (sheep.location == wolf.location) {
                System.out.println("The sheep is dead :(");
                System.exit(1);
            } else {
                if (sheep.location.x == sheep.location.y) {
                    sheep.setBehaviour(new StandStill());
                    shepherd.setBehaviour(new MoveTowards(sheep));
                }

               allCharacters.forEach((c) -> c.aiMove(this).perform());
                    player.startMove();
                timeOfLastMove = Instant.now();
            }
        }
        blocksAI(sheep);
        blocksAI(shepherd);
        blocksAI(wolf);
    }

    public void paint(Graphics g, Point mouseLocation) {

        grid.paint(g, mouseLocation);
      for(int i = 0; i < blocks.length; i++){
          blocks[i].paint(g);

        }

        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
        player.paint(g);


    }
    public  Memento getMemento(Memento m) {

        return m;
    }

    public Memento createMemento() {

        return new Memento(sheep.getLocationOf(), shepherd.getLocationOf(), wolf.getLocationOf(), player.location);
    }

    public void setMemento(Memento m) {
            sheep.setLocationOf(m.sheep);
            shepherd.setLocationOf(m.shepherd);
            wolf.setLocationOf(m.wolf);
            player.location = m.player;
        }


        public void blocksAI(Character c){
        ArrayList<String> cannotMove = new ArrayList<>();
        //System.out.println(getMoves(c));
            for(int i = 0; i < blocks.length; i++) {
                Cell aboveChar = grid.above(c.location).get();
                Cell belowChar = grid.below(c.location).get();
                Cell leftOfChar = grid.leftOf(c.location).get();
                Cell rightOfChar = grid.rightOf(c.location).get();
                String nextMove = "";
                if(getMoves(c).size() > 0)
                nextMove = getMoves(c).get(0).toString();

                if(aboveChar.equals(blocks[i].location)) {
                    cannotMove.add("up");
              //find the next move; if next move is X
                    if(nextMove.contains("Up")){
                        c.setBehaviour( new StandStill());
                          System.out.println(c + "can't move up");
                    }
                           // System.out.println("Sheep cannot move up!");
//                            if(c.behaviour.getMoves().get(0).toString().contains("Up")){
//
//                            }
//                    if(c.behaviour.chooseMove(this, c).equals(new MoveUp(grid, c))){
//
//                    }
                }
                if(belowChar.equals(blocks[i].location)) {
                    cannotMove.add("down");
//                    System.out.println("Sheep cannot move down!");
                    if(nextMove.contains("Down")){
                        c.setBehaviour( new StandStill());
                         System.out.println(c + "can't move down");
                    }
                }
                if(leftOfChar.equals(blocks[i].location)) {
                    cannotMove.add("left");
//                    System.out.println("Sheep cannot move left!");
                    if(nextMove.contains("Left")){
                        c.setBehaviour( new StandStill());
                          System.out.println(c + "can't move left");
                    }
                }

                if(rightOfChar.equals(blocks[i].location)) {
                    cannotMove.add("right");
                    if(nextMove.contains("Right")){
                        c.setBehaviour( new StandStill());
                   System.out.println(c + "can't move right");
                    }
//                    System.out.println("Sheep cannot move right!");
//                    System.out.println(sheep.behaviour);
//                    RelativeMove CharMove = getMoves(c).get(0);
//                    RelativeMove right = new MoveRight(grid, c);
//                    c.behaviour.chooseMove(this, c);
//                    if(getMoves(c).get(0) instanceof MoveRight){
//                    System.out.println(c + " wants to move right!");
//                    }
                }
            }
            if(cannotMove.size() == 0){
//                System.out.println(c + "can move wherever!");
            }
            else {
//                System.out.println(c + " cannot move " + cannotMove.toString());
            }
        }


        public ArrayList<RelativeMove> getMoves(Character c){
        ArrayList<RelativeMove> myMoves = new ArrayList<>();
        if(c.behaviour.getMoves()!=null) {
            for (RelativeMove r : c.behaviour.getMoves()) {
                myMoves.add(r);
            }
        }
          //  myMoves = c.behaviour.getMoves();
//        return c.behaviour.getMoves();
            else {

        }
            return myMoves;
        }
    }


