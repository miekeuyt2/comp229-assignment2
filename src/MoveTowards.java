import bos.NoMove;
import bos.RelativeMove;


import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;

public class MoveTowards implements Behaviour {
    Character target;

    public MoveTowards(Character character){
        this.target = character;
    }

List<RelativeMove> moves;
    public RelativeMove chooseMove(Stage stage, Character mover) {
//   List<RelativeMove> movesToBlock = stage.grid.movesBetween(mover.location, block.location,mover);
        List<RelativeMove> movesToTarget = stage.grid.movesBetween(mover.location,target.location, mover);
        moves = movesToTarget;
//        if(movesToBlock.size() == 1){
//            return movesToBlock.get(0).oppositeMove();

//        }
        if (movesToTarget.size() == 0)
            return new NoMove(stage.grid, mover);
        else
            return movesToTarget.get(0);    }

            public List<RelativeMove> getMoves() {
            return moves;
            }

}
