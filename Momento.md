
My implementation of the Momento Pattern is like this:
The Stage class has getMemento, setMemento and createMemento functions.
createMemento returns a new memento containing the locations of the characters and player, and setMemento then returns these values from the saved memento back to the stage.
The caretaker listens out for the key press, and then calls upon the Stage class' methods. Once creating a new memento, the caretaker saves this memento in SavedMemento, and then uses this variable in calling setMemento, after checking that it's not null.

My implementation closely follows the standard momento implementation, with the Stage class acting as the originator, and the caretaker and Memento being its own classes.

However, as a save mechanism, using the Memento pattern in this way is quite useless, as the save states are deleted upon exiting the program. In order to have consistent saving and loading in between closing and opening the program, perhaps it would be wiser to write to a file the locations of the gameObjects, which can then be read when loading is necessary, but that would then ruin the point of the memento pattern.

_